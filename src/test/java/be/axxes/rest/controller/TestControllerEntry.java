package be.axxes.rest.controller;

import org.junit.Test;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestControllerEntry extends BaseControllerIT {

    @Test
    public void testGetEntry() throws Exception {
        performGetJson("/entries/1")
                .andExpect(status().isOk())
                .andExpect(jsonPath(".id").value("1"))
                .andExpect(jsonPath(".value").value("value of 1"))
        ;
    }

    @Test
    public void testNotFound() throws Exception {
        performGetJson("/entries/unknownId")
                .andExpect(status().isNotFound())
        ;
    }
}
